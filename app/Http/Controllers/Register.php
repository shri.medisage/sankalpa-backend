<?php

namespace App\Http\Controllers;

use App\Models\CityState;
use App\Models\Members;
use App\Models\Workplace;
use Illuminate\Http\Request;

class Register extends Controller
{
    public function register(Request $request)
    {

        //Validate Errors
        try {
            $request->validate([
                'fname' => 'bail|required|max:25|regex:/^[a-zA-Z]+$/',
                'lname' => 'bail|required|max:25|regex:/^[a-zA-Z]+$/',
                'mobile_number' => 'bail|required|regex:/^\\+?\\d{1,4}?[-.\\s]?\\(?\\d{1,3}?\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,9}$/|digits_between:10,10',
                'email' => 'bail|required|regex:/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
                'state' => "bail|required|exists:city_state",
                'city' => 'bail|required|exists:city_state',
                'designation' => 'required',
                'hospital' => 'required',
            ], [
                'fname.max' => 'First name may not be greater than 25',
                'lname.max' => 'Last name may not be greater than 25',
                'fname.required' => 'The first name field is required',
                'lname.required' => 'The last name field is required',
                'fname.regex' => 'The first name must not contain special characters',
                'lname.regex' => 'The last name must not contain special characters',
                'mobile_number.digits_between' => 'The mobile number must be 10 characters long',
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json([
                "Status" => false,
                'errors' => $e->errors(),
            ], 400);
        }

        $existingMember = Members::where('mobile_number', $request->mobile_number)->first();

        if ($existingMember) {
            // Member already exists, update the timestamp
            $existingMember->updated_at = now();
            $existingMember->register_type = 'sankalp_reregistration';
            $existingMember->save();

            return response()->json([
                "Status" => true,
                'message' => "Member already exists. Timestamp updated.",
            ], 200);
        }

        $post = new Members;
        $post->fname = $request->input('fname');
        $post->lname = $request->input('lname');
        $post->mobile_number = $request->input('mobile_number');
        $post->email = $request->input('email');
        $post->member_type = $request->input('designation');
        $post->register_type = 'sankalp_reregistration';
        $post->city = $request->input('city');
        $post->state = $request->input('state');
        $post->country = "India";
        $post->country_code = "91";
        $post->is_profile_complete = 0;
        $post->save();

        $user_data = Members::where('mobile_number', $request->mobile_number)->first();
        $member_id = $user_data->id;

        //Store Data into member_workplace_info table
        $Workplace = new Workplace;
        $Workplace->member_id = $member_id;
        $Workplace->member_workplace_info_ref_no = rand(1, 100);
        $Workplace->designation = $user_data->member_type;
        $Workplace->city = $user_data->city;
        $Workplace->place_of_work = $request->input('hospital');
        $Workplace->save();

        return response()->json([
            "Status" => true,
            'message' => "Registered Successfully",
        ], 200);
    }

    //Function for getting the list of city
    public function getCity($state)
    {
        if ($state) {
            $ci = [];
            $ci2 = [];
            $cities = CityState::where('state', $state)->get();
            foreach ($cities as $city) {
                array_push($ci, trim($city->city));
            }
            $ci2 = array_unique($ci);
            return response()->json($ci2);
        } else {
            return response()->json(["Message" => "No state selected"]);
        }
    }

    //Function for getting the list of state
    public function getState()
    {
        $sta = [];
        $sta2 = [];
        $states = CityState::where('country', 'India')->get();
        foreach ($states as $state) {
            array_push($sta, trim($state->state));
        }
        $sta2 = array_unique($sta);
        return response()->json($sta2);
    }
}
