<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityState extends Model
{
    protected $table = "city_state";
    use HasFactory;
}
